IF NOT EXIST venv\NUL (
	up.bat
	)

IF EXIST coverage_html\NUL (
	rmdir /s /q coverage_html 
	) ELSE (
	echo NO
	)

cmd /k " cd venv/Scripts & activate & cd ../.. & cover.bat"