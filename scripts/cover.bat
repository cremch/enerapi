cd api/src
del /S *.pyc
coverage run -m teamcity.unittestpy discover
coverage html -d ../../coverage_html

cd ../..

call venv\Scripts\deactivate