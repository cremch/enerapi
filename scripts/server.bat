IF NOT EXIST venv\NUL (
	up.bat
	)

cmd /k " call venv/Scripts/activate & cd api/src & del /S *.pyc & coverage run -m teamcity.unittestpy discover & call ../../venv/Scripts/deactivate.bat & cd ../.."