echo Installing virtualenv
pip install virtualenv

echo Creating virtual environment
call virtualenv venv
call .\venv\scripts\activate

echo Installing dependencies
call pip install -r requirements.txt

echo Initializing swagger-codegen git repository
git submodule init
git submodule update

cd swagger-codegen
echo Checkout version v2.1.4 of Swagger-codegen
git checkout v2.1.4


echo Building swagger-codegen
call mvn package
cd ..

echo Starting api
start python api/src/main.py

echo Building client code
call scripts\codegen

echo Testing api
python -m unittest discover
call venv\Scripts\deactivate