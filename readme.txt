La version française est disponible ci-dessous.

Requirements:
1. Install the last version of python 2.7
2. Add the (local) environment variable 
ENERAPI_DATA_DIR: <with the path to crem-api\api\data folder>

Only for Windows:
Install at least Java 7 (jdk1.7) and add the environment variable
JAVA_HOME: <Your Java directory>
PATH: add JAVA_HOME/bin

Then install Maven:
https://maven.apache.org/
Also add the maven folder into your systems PATH

3. Run "user" script, if you are on Linux based system or "user.bat" for Windows

It will install the development environment and start the api once it is done.

For running the api manually:
python src/main.py

Deployment:
Read this article for a howto to deploy a Flask app on Heroku with gunicorn

https://devcenter.heroku.com/articles/getting-started-with-python-o

TeamCity is also configured to push changes to heroku when a build passes.

Thank you for trying out our API. We are happy to hear about your feedback!

Français: 
Pré-requis:

1. Installer python 2.7
2. Ajouter la variables d'environnement
ENERAPI_DATA_DIR: <with the path to crem-api\api\data folder>

Pour Windows:
Installer Java 7 minimum (jdk1.7)
Poser les variables d'environnements:
JAVA_HOME: <répertoire d'installation de java>
PATH: ajouter JAVA_HOME/bin

Installer Maven:
https://maven.apache.org/
Ajouter le dossier bin de maven au PATH

3. Lancer user.bat

Ce script compile et setup l'environnement de travail.
Il lance aussi l'api une fois prête. Elle peut etre 
arrêtée à souhait. Pour la relancer, taper:

python src/main.py


#################################################################################
How to generate a client from the api:
run api locally or point script towards http://enerapi.herokuapp.com/swagger.json

mkdir gen
mkdir gen/python

java -jar ../swagger-codegen/modules/swagger-codegen-cli/target/swagger-codegen-cli.jar generate \
  -i http://localhost:5000/swagger.json \
  -l python \
  -o gen/python

Change directory to enerapi/scripts and run ./codegen in a terminal