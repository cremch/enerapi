from config import Config
from common.injection import *
from common.DomainException import DomainException

# from resources.OhmRes import *
from resources.DetailedBuildingGeometryRes import *
from resources.BoilerBehaviourRes import *
from resources.CompositeCurveRes import *
from resources.EquivalentSouthAreaRes import *
from resources.BuildingGeometryRes import *
from resources.GeoThermalRes import *
from resources.HeatExchangerRes import *
from resources.HeatNetworkRes import *
from resources.HeatPowerSupplyTempRes import *
from resources.HeatPumpRes import *
from resources.HotWaterTankRes import *
from resources.MaximumLegalHeatingDemandRes import *
from resources.StaticPowerRes import *
from resources.SolarFunctionIncidentAngleRes import *
from resources.SolarFunctionRadianceOntoTiltedPlaneRes import *
from resources.SolarPVRes import *
from resources.SolarThermalProductionFromRadiationRes import *
from resources.SolarThermalRes import *
from resources.SpecificEnergyRequirementsRes import *
from resources.StratifiedDynamicRes import *

if __name__ == '__main__':
    Config.app.run(debug=True, host="localhost")
