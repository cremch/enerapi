from business.Storage.ThermalStorage.StratifiedDynamic import StratifiedDynamic
from common.DomainException import DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg, help=StratifiedDynamic.help_calculate())

strat_dyn_fields = Config.api.model("stratDynModel", {
    'temperatures_layer': fields.List(fields.List(fields.Float)),
    'temperatures_step': fields.List(fields.List(fields.Float))
})


@Config.api.route("/Storage/ThermalStorage/StratifiedDynamic/calculate")
class StratifiedDynamicRes(Resource):
    @Config.api.doc(parser=parser, description=StratifiedDynamic.help())
    @Config.api.marshal_with(strat_dyn_fields)
    def post(self):
        args = parser.parse_args()
        try:
            ret = StratifiedDynamic(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
