from business.EnergyConversion.HeatExchanger.HeatExchanger import HeatExchanger
from common.DomainException import DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=HeatExchanger.help_calculate())

heatEx = Config.api.model("heatExModel", {
    't_out_hot': fields.List(fields.Float),
    't_out_cold': fields.List(fields.Float)
})


@Config.api.route("/EnergyConversion/HeatExchanger/calculate")
class HeatExchangerRes(Resource):
    @Config.api.doc(parser=parser, description=HeatExchanger.help())
    @Config.api.marshal_with(heatEx)
    def post(self):
        args = parser.parse_args()
        try:
            ret = HeatExchanger(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400