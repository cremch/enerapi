from business.EnergyConversion.SolarThermal.SolarThermal import SolarThermal
from common.DomainException import DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=SolarThermal.help_calculate())

solarTh = Config.api.model("solarThModel", {
    'usefulOutputPower': fields.Float,
    'IAMbeam': fields.Float,
    'IAMdiffuse': fields.Float
})


@Config.api.route("/EnergyConversion/SolarThermal/calculate")
class SolarThermalRes(Resource):
    @Config.api.doc(parser=parser, description=SolarThermal.help())
    @Config.api.marshal_with(solarTh)
    def post(self):
        args = parser.parse_args()
        try:
            ret = SolarThermal(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
