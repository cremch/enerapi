from business.EnergyConversion.HeatPump.HeatPump import HeatPump
from common.DomainException import DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=HeatPump.help_calculate())

hp_fields = Config.api.model("hpModel", {
    'cop_real': fields.List(fields.Float),
    'cop_carnot': fields.List(fields.Float)
})


@Config.api.route("/EnergyConversion/HeatPump/calculate")
class HeatPumpRes(Resource):
    @Config.api.doc(parser=parser, description=HeatPump.help())
    @Config.api.marshal_with(hp_fields)
    def post(self):
        args = parser.parse_args()
        try:
            ret = HeatPump(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
