from business.EnergyConversion.SolarPV.SolarPV import SolarPV, DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=SolarPV.help_calculate())

solarPV = Config.api.model("solarPVModel", {
    'ElectricOutputPower': fields.Float
})


@Config.api.route("/EnergyConversion/SolarPV/calculate")
class SolarPVRes(Resource):
    @Config.api.doc(parser=parser, description=SolarPV.help())
    @Config.api.marshal_with(solarPV)
    def post(self):
        args = parser.parse_args()
        try:
            ret = SolarPV(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
