from business.EnergyConversion.Boiler.BoilerBehaviour import BoilerBehaviour
from common.DomainException import DomainException
from common.jsonArg import *
from config import *
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg, help=BoilerBehaviour.help_calculate())

boiler_fields = Config.api.model("BoilerModel", {
    'p_supplied': fields.Float,
    'p_consumed': fields.Float
})


@Config.api.route("/EnergyConversion/Boiler/calculate")
class BoilerBehaviourRes(Resource):
    @Config.api.doc(parser=parser, description=BoilerBehaviour.help())
    @Config.api.marshal_with(boiler_fields)
    def post(self):
        args = parser.parse_args()
        try:
            ret = BoilerBehaviour(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
