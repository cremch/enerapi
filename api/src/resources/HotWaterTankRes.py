from business.Storage.HotWaterTank.HotWaterTank import HotWaterTank
from common.DomainException import DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg, help=HotWaterTank.help_calculate())

hot_water_tank_fields = Config.api.model("hotWaterTankModel", {
    't': fields.List(fields.Float),
})


@Config.api.route("/Storage/HotWaterTank/calculate")
class HotWaterTankRes(Resource):
    @Config.api.doc(parser=parser, description=HotWaterTank.help())
    @Config.api.marshal_with(hot_water_tank_fields)
    def post(self):
        args = parser.parse_args()
        try:
            ret = HotWaterTank(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
