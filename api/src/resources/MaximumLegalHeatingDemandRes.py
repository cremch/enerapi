from business.BuildingDemand.MaximumLegalHeatingDemand import MaximumLegalHeatingDemand, DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=MaximumLegalHeatingDemand.help_calculate())

qhli_fields = Config.api.model("QhliModel", {
    'Qhli': fields.Float,
    'kWh_per_m2': fields.Float})


@Config.api.route("/Building/MaximumLegalHeatingDemand/calculate")
class MaximumLegalHeatingDemandRes(Resource):
    @Config.api.doc(parser=parser, description=MaximumLegalHeatingDemand.help())
    @Config.api.marshal_with(qhli_fields)
    def post(self):
        args = parser.parse_args()
        try:
            ret = MaximumLegalHeatingDemand(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
