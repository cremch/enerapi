from business.GeoBuilding.BuildingGeometry import BuildingGeometry, DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg, help=BuildingGeometry.help_calculate())

geob_fields = Config.api.model("GeoBuildModel", {
    'heated_surface': fields.Float,
    'heated_volume': fields.Float
})


@Config.api.route("/Building/Geometry/calculate")
class BuildingGeometryRes(Resource):
    @Config.api.doc(parser=parser, description=BuildingGeometry.help())
    @Config.api.marshal_with(geob_fields)
    def post(self):
        args = parser.parse_args()
        try:
            ret = BuildingGeometry(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
