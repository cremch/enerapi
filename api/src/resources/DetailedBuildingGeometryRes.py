from business.GeoBuilding.DetailedBuildingGeometry import DetailedBuildingGeometry
from common.DomainException import DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=DetailedBuildingGeometry.help_calculate())

geoB = Config.api.model("geoBModel", {
    'wall_len': fields.List(fields.Float),
    'ThBr_inter_floor': fields.List(fields.Float),
    'wall_surf': fields.List(fields.Float),
    'orientation': fields.List(fields.Float),
    'window': fields.List(fields.Integer),
    'win_surf': fields.List(fields.Float),
    'win_length': fields.List(fields.Float),
    'win_height': fields.List(fields.Float)
})


@Config.api.route("/Building/DetailedGeometry/calculate")
class DetailedBuildingGeometryRes(Resource):
    @Config.api.doc(parser=parser, description=DetailedBuildingGeometry.help())
    @Config.api.marshal_with(geoB)
    def post(self):
        args = parser.parse_args()
        try:
            ret = DetailedBuildingGeometry(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
