from business.NaturalResources.GeoThermal.GeoThermal import GeoThermal
from common.DomainException import DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg, help=GeoThermal.help_calculate())

geoTh = Config.api.model("GeoThModel", {
    'Geothermal specific power': fields.Float,
    'Geothermal power': fields.Float,
    'Geothermal energy': fields.Float
})


@Config.api.route("/Resources/GeoThermal/calculate")
class GeoThermalRes(Resource):
    @Config.api.doc(parser=parser, description=GeoThermal.help())
    @Config.api.marshal_with(geoTh)
    def post(self):
        args = parser.parse_args()
        try:
            ret = GeoThermal(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
