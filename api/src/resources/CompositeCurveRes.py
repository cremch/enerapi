from config import Config
from common.jsonArg import jsonArg
from business.CompositeCurve.CompositeCurve import CompositeCurve, DomainException
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg, help=CompositeCurve.help_calculate())

cc_fields = Config.api.model("ccModel", {
    'temp_values': fields.List(fields.Float),
    'p_cold_stream': fields.List(fields.Float),
    'p_hot_stream': fields.List(fields.Float),
    't_pinch': fields.Float
})


@Config.api.route("/EnergySystemDesign/CompositeCurve/calculate")
class CompositeCurveRes(Resource):
    @Config.api.doc(parser=parser, description=CompositeCurve.help())
    @Config.api.marshal_with(cc_fields)
    def post(self):
        args = parser.parse_args()
        try:
            ret = CompositeCurve(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
