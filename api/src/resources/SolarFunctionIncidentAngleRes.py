from business.NaturalResources.Solar.SolarFunctionIncidentAngle import SolarFunctionIncidentAngle, DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=SolarFunctionIncidentAngle.help_calculate())

solarIA = Config.api.model("solarIAModel", {
    'zenithAngle': fields.Float,
    'solarAzimuthAngle': fields.Float,
    'incidentAngle': fields.Float,
    'longIncidentAngle': fields.Float,
    'tranIncidentAngle': fields.Float
})


@Config.api.route("/Resources/Solar/IncidentAngle/calculate")
class SolarFunctionIncidentAngleRes(Resource):
    @Config.api.doc(parser=parser, description=SolarFunctionIncidentAngle.help())
    @Config.api.marshal_with(solarIA)
    def post(self):
        args = parser.parse_args()
        try:
            ret = SolarFunctionIncidentAngle(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
