from config import Config
from common.jsonArg import jsonArg
from business.BuildingDemand.SpecificEnergyRequirements import SpecificEnergyRequirements, DomainException
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=SpecificEnergyRequirements.help_calculate())

specNeeds = Config.api.model("specNeedsModel", {
    'hS': fields.Float,
    'hW': fields.Float,
    'elec': fields.Float,
    'h_full_ch': fields.Integer
})


@Config.api.route("/Building/SpecificEnergyRequirements/calculate")
class SpecNeedsHFCRes(Resource):
    @Config.api.doc(parser=parser, description=SpecificEnergyRequirements.help())
    @Config.api.marshal_with(specNeeds)
    def post(self):
        args = parser.parse_args()
        try:
            ret = SpecificEnergyRequirements(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
