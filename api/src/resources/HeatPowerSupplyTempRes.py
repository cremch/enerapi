from config import Config
from common.jsonArg import jsonArg
from business.BuildingDemand.HeatPowerSupplyTemp import HeatPowerSupplyTemp, DomainException
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=HeatPowerSupplyTemp.help_calculate())

heatPsuppT = Config.api.model("heatPSuppTModel", {
    'p_heating': fields.List(fields.Float),
    't_supply': fields.List(fields.Float),
    't_return': fields.List(fields.Float),
    'p_installed': fields.Float
})


@Config.api.route("/Building/HeatPowerSupplyTemp/calculate")
class HeatPowerSupplyTempRes(Resource):
    @Config.api.doc(parser=parser, description=HeatPowerSupplyTemp.help())
    @Config.api.marshal_with(heatPsuppT)
    def post(self):
        args = parser.parse_args()
        try:
            ret = HeatPowerSupplyTemp(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
