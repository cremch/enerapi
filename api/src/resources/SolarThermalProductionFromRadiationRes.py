from business.EnergyConversion.SolarThermal.SolarThermalProductionFromRadiation import SolarThermalProductionFromRadiation, \
    DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=SolarThermalProductionFromRadiation.help_calculate())


@Config.api.route("/EnergyConversion/SolarThermalProductionFromRadiation/calculate")
class SolarThermalProductionFromRadiationRes(Resource):
    @Config.api.doc(parser=parser, description=SolarThermalProductionFromRadiation.help())
    def post(self):
        args = parser.parse_args()
        try:
            ret = SolarThermalProductionFromRadiation(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
