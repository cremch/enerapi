from business.GeoBuilding.EquivalentSouthArea import EquivalentSouthArea, DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg, help=EquivalentSouthArea.help_calculate())

esa_fields = Config.api.model("esaModel", {
    'esa': fields.Float
})


@Config.api.route("/Building/EquivalentSouthArea/calculate")
class EquivalentSouthAreaRes(Resource):
    @Config.api.doc(parser=parser, description=EquivalentSouthArea.help())
    @Config.api.marshal_with(esa_fields)
    def post(self):
        args = parser.parse_args()
        try:
            ret = EquivalentSouthArea(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
