from business.NaturalResources.Solar.SolarFunctionRadianceOntoTiltedPlane import SolarFunctionRadianceOntoTiltedPlane, \
    DomainException
from common.jsonArg import jsonArg
from config import Config
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=SolarFunctionRadianceOntoTiltedPlane.help_calculate())

solarROTP = Config.api.model("solarROTPModel", {
    'GbeamTitledPlane': fields.Float,
    'GdiffuseTiltedPlane': fields.Float,
    'GtotalTiltedPlane': fields.Float
})


@Config.api.route("/Resources/Solar/RadianceOnTiltedPlane/calculate")
class SolarFunctionRadianceOntoTiltedPlaneRes(Resource):
    @Config.api.doc(parser=parser, description=SolarFunctionRadianceOntoTiltedPlane.help())
    @Config.api.marshal_with(solarROTP)
    def post(self):
        args = parser.parse_args()
        try:
            ret = SolarFunctionRadianceOntoTiltedPlane(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
