from config import Config
from flask_restplus import Resource

parser = Config.api.parser()
parser.add_argument("u", type=float, location="form")
parser.add_argument("r", type=float, location="form")
parser.add_argument("i", type=float, location="form")


@Config.api.route("/resources/ohm")
class Ohm(Resource):
    @Config.api.doc(parser=parser)
    def post(self):
        args = parser.parse_args()

        try:
            return Ohm.calc(args), 200
        except Exception as e:
            return e.message, 400
