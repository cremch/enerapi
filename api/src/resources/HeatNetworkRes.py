from config import Config
from common.jsonArg import jsonArg
from business.DistributionNetwork.HeatNetwork import HeatNetwork, DomainException
from flask_restplus import Resource, fields

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg, help=HeatNetwork.help_calculate())

hnet_fields = Config.api.model("hnetModel", {
    'p_supply': fields.Float,
    'p_th_supply': fields.Float,
    'p_th_return': fields.Float,
    'ratio_th_loss': fields.Float,
    't_supply': fields.Float,
    't_reject': fields.Float,
    'fluid_flow': fields.Float,
    'inner_diameter_min': fields.Float
})


@Config.api.route("/Network/HeatNetwork/calculate")
class HeatNetworkRes(Resource):
    @Config.api.doc(parser=parser, description=HeatNetwork.help())
    @Config.api.marshal_with(hnet_fields)
    def post(self):
        args = parser.parse_args()
        try:
            ret = HeatNetwork(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
