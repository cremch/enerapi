from flask_restplus import Resource, fields
from business.BuildingDemand.StaticPower import StaticHeatingCoolingPower, DomainException
from common.jsonArg import jsonArg
from config import Config

parser = Config.api.parser()
parser.add_argument("properties", required=True, location="form", type=jsonArg,
                    help=StaticHeatingCoolingPower.help_calculate())

Param_build = Config.api.model("Building", {
    'Penvelope_heating': fields.List(fields.Float),
    'Penvelope_cooling': fields.List(fields.Float),
    'Psol': fields.List(fields.Float),
    'Pocc': fields.List(fields.Float),
    'Ptot_heating': fields.List(fields.Float),
    'Ptot_cooling': fields.List(fields.Float)
})


@Config.api.route("/Building/StaticPower/calculate")
class StaticHeatingCoolingPowerRes(Resource):
    @Config.api.doc(parser=parser, description=StaticHeatingCoolingPower.help())
    @Config.api.marshal_with(Param_build)
    def post(self):
        args = parser.parse_args()
        try:
            ret = StaticHeatingCoolingPower(args.properties).calculate()
            return ret, 200
        except DomainException as e:
            print e
            return e.message, 400
