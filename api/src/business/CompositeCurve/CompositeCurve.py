from business.Base.Base import Base
from common.Guard import *


class CompositeCurve(Base):
    """
    Build composite curve from input and output flows temperature and from associated power.
    """

    @staticmethod
    def help():
        return CompositeCurve.__doc__ + "\r\n" + CompositeCurve.calculate.__doc__

    @staticmethod
    def help_calculate():
        return CompositeCurve.__init__.__doc__.format()

    def __init__(self, args):
        """
        Arguments should be an object of the form:

        {{"t_in": [20, 170, 80, 150], "t_out": [135, 60, 140, 30],
            "power": [230, 330, 240, 180], "dt_min": 5}}

        {{
            "t_in":         [deg.C]     (list)
            "t_out":        [deg.C]     (list)
            "power":        [kW]        (list)      (must be positive)
            "dt_min":       [deg.C]                 (must be positive)
        }}
        """

        super(CompositeCurve, self).__init__(args)

        Guard.check_if_key_in_dict("t_in", args)
        Guard.check_if_key_in_dict("t_out", args)
        Guard.check_if_key_in_dict("power", args)
        Guard.check_if_key_in_dict("dt_min", args)

        Guard.check_is_higher(args["dt_min"], lower_limit=0, strict=True)

        Guard.check_for_same_list_lengths(l1=args["t_in"], l2=args["t_out"], l3=args["power"])

        Guard.check_for_every_item_of_list(args["power"], Guard.check_is_higher, lower_limit=0)

        self.args = args

    @staticmethod
    def cum_sum(it):
        total = 0
        for x in it:
            total += x
            yield total

    def calculate(self):
        """
        The returned object is of the form:
        {
            "temp_values":      [deg]       (list)
            "p_cold_stream":    [kW]        (list)
            "p_hot_stream":     [kW]        (list)
            "t_pinch":          [deg]
        }

        Detailed Description:

        *********************************************************************
        Inputs:
        *********************************************************************
        t_in            [deg]       (list)      Flow input temperature
        t_out           [deg]       (list)      Flow output temperature
        power           [kW]        (list)      Power transmitted to the flow
        dt_min          [deg]                   Minimal pinch temperatures delta

        *********************************************************************
        Outputs:
        *********************************************************************
        temp_values     [deg]       (list)      Temperature value list
        p_cold_stream   [kW]        (list)      Associated power values for cold stream aggregated curve
        p_hot_stream    [kW]        (list)      Associated power values for hot stream aggregated curve
        t_pinch         [deg]                   Pinch temperature

        *********************************************************************
        Reference:
        *********************************************************************
        Ian C.Kemp. "Pinch Analysis and Process Integration", Second Edition, 2007, Elsevier Ltd.

        """

        # Create cp_list [kW/deg]
        cp_list = [float(p) / (t_out - t_in) for p, t_in, t_out in
                   zip(self.args["power"], self.args["t_in"], self.args["t_out"])]

        # Creating shifted temperatures level
        t_in_shift = []
        t_out_shift = []

        for cp, t_in, t_out in zip(cp_list, self.args["t_in"], self.args["t_out"]):
            if cp >= 0:
                t_in += self.args["dt_min"] / 2.0
                t_out += self.args["dt_min"] / 2.0
            else:
                t_in -= self.args["dt_min"] / 2.0
                t_out -= self.args["dt_min"] / 2.0

            t_in_shift.append(t_in)
            t_out_shift.append(t_out)

        # Create temperatures level list
        temperatures = t_in_shift + t_out_shift

        temp_values = list(set(temperatures))
        temp_values.sort()

        # Build heat and cold stream aggregated curves
        p_cold_stream = [0] * len(temp_values)
        p_hot_stream = [0] * len(temp_values)

        for t_ind, t in enumerate(temp_values):
            for cp_ind, cp in enumerate(cp_list):
                if cp >= 0:
                    if t_in_shift[cp_ind] < t <= t_out_shift[cp_ind]:
                        p_cold_stream[t_ind] += (t - temp_values[t_ind - 1]) * cp

                else:
                    if t_out_shift[cp_ind] < t <= t_in_shift[cp_ind]:
                        p_hot_stream[t_ind] += (temp_values[t_ind - 1] - t) * cp

        p_cold_stream = list(CompositeCurve.cum_sum(p_cold_stream))
        p_hot_stream = list(CompositeCurve.cum_sum(p_hot_stream))

        # Create grand composite curve in order to find pinch point
        p_grand_composite = [0] * len(temp_values)
        for idx, t in enumerate(temp_values):
            p_grand_composite[idx] = p_cold_stream[idx] - p_hot_stream[idx]

        # Determine shift and pinch temperature
        shift = min(p_grand_composite)
        index_t_pinch = p_grand_composite.index(shift)
        t_pinch = temp_values[index_t_pinch]

        # Shift cold stream aggregated curve from shift in order to fit minimal pinch
        p_cold_stream_shifted = [0] * len(p_cold_stream)
        for idx, p in enumerate(p_cold_stream):
            p_cold_stream_shifted[idx] = p_cold_stream[idx] - shift

        return {
            "temp_values": temp_values,
            "p_cold_stream": p_cold_stream_shifted,
            "p_hot_stream": p_hot_stream,
            "t_pinch": t_pinch
        }
