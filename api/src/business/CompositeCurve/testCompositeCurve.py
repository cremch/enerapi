import unittest
from business.CompositeCurve.CompositeCurve import CompositeCurve
from common.DomainException import DomainException


class TestCompositeCurve(unittest.TestCase):
    def test_if_all_arguments_are_here_no_exception_should_be_raised(self):
        args = {"t_in": [0], "t_out": [100], "power": [100], "dt_min": 5}
        CompositeCurve(args)

    def test_if_any_required_argument_is_missing_an_exception_should_be_raised(self):
        args = {"t_out": [100], "power": [100], "dt_min": 5}
        with self.assertRaises(DomainException):
            CompositeCurve(args)

        args = {"t_in": [0], "power": [100], "dt_min": 5}
        with self.assertRaises(DomainException):
            CompositeCurve(args)

        args = {"t_in": [0], "t_out": [100], "dt_min": 5}
        with self.assertRaises(DomainException):
            CompositeCurve(args)

        args = {"t_in": [0], "t_out": [100], "power": [100]}
        with self.assertRaises(DomainException):
            CompositeCurve(args)

    def test_if_a_list_argument_has_an_incorrect_length_an_exception_should_be_raised(self):
        args = {"t_in": [0, 1], "t_out": [100], "power": [100], "dt_min": 5}
        with self.assertRaises(DomainException):
            CompositeCurve(args)

        args = {"t_in": [0], "t_out": [100, 101], "power": [100], "dt_min": 5}
        with self.assertRaises(DomainException):
            CompositeCurve(args)

        args = {"t_in": [0], "t_out": [100], "power": [100, 101], "dt_min": 5}
        with self.assertRaises(DomainException):
            CompositeCurve(args)

    def test_if_an_argument_has_an_incorrect_value_an_exception_should_be_raised(self):
        args = {"t_in": [0, 50], "t_out": [100, 100], "power": [100, -100], "dt_min": 5}
        with self.assertRaises(DomainException):
            CompositeCurve(args)

        args = {"t_in": [0, 50], "t_out": [100, 100], "power": [100, 100], "dt_min": -5}
        with self.assertRaises(DomainException):
            CompositeCurve(args)

    def test_the_estimation_should_be_correct_case1(self):
        args = {"t_in": [20, 170, 80, 150], "t_out": [135, 60, 140, 30], "power": [230, 330, 240, 180], "dt_min": 10}

        expected = {
            "temp_values": [25, 55, 85, 140, 145, 165],
            "p_hot_stream": [0, 45, 180, 427.5, 450, 510],
            "p_cold_stream": [60, 120, 180, 510, 530, 530],
            "t_pinch": 85
        }

        actual = CompositeCurve(args).calculate()

        self.assertAlmostEqual(expected["t_pinch"], actual["t_pinch"], places=1, msg=None, delta=None)

        for t in range(0, len(expected["temp_values"])):

            self.assertAlmostEqual(expected["temp_values"][t], actual["temp_values"][t], places=1, msg=None, delta=None)
            self.assertAlmostEqual(expected["p_hot_stream"][t], actual["p_hot_stream"][t], places=1, msg=None, delta=None)
            self.assertAlmostEqual(expected["p_cold_stream"][t], actual["p_cold_stream"][t], places=1, msg=None, delta=None)


