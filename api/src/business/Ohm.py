class Ohm(object):
    """docstring for LoiOhm"""

    @staticmethod
    def calc(args):

        tab = [arg for arg in args if args[arg] is not None]
        if len(tab) < 2:
            raise Exception("Missing arguments")
        if len(tab) >= 3:
            raise Exception("Too many arguments")

        if args["u"] is None:
            args["u"] = args["r"] * args["i"]

        if args["r"] is None:
            args["r"] = args["u"] / args["i"]

        if args["i"] is None:
            args["i"] = args["u"] / args["r"]

        return args
