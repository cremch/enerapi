from business.Base.Base import Base
from common.Guard import *

__author__ = 'ppuerto'


class BuildingGeometry(Base):
    """
    Estimate heated surface and heated volume of a building with ground surface, number of floors and height of floors.
    """

    _default_parameter_value = {"ratio_heated_surface": 0.8, "floor_height": 2.5}

    @staticmethod
    def help():
        return BuildingGeometry.__doc__ + "\r\n" + BuildingGeometry.calculate.__doc__

    @staticmethod
    def help_calculate():
        return BuildingGeometry.__init__.__doc__.format(
            BuildingGeometry._default_parameter_value["ratio_heated_surface"],
            BuildingGeometry._default_parameter_value["floor_height"]
        )

    def __init__(self, args):
        """
        Arguments should be an object of the form:

        {{"g_surf": 50, "nbr_floor": 2}}

        {{
            "g_surf":                   [m2]
                (must be positive)
            "nbr_floor":                [int.]
                (must be a positive integer)
            "ratio_heated_surface":     [ratio]
                (float bigger than 0 and smaller or equal to 1,
                default value: {0})
            "floor_height":             [m]
                (must be positive and above 2m, default value: {1})
        }}
        """

        super(BuildingGeometry, self).__init__(args)

        Guard.check_if_key_in_dict("g_surf", args)
        Guard.check_if_key_in_dict("nbr_floor", args)

        Guard.check_is_higher(args["g_surf"], lower_limit=0)
        Guard.check_is_higher(args["nbr_floor"], lower_limit=0)

        Guard.check_value_in_between(args["ratio_heated_surface"], min=0, max=1, min_in=False)
        Guard.check_value_in_between(args["floor_height"], min=2, max=100)

        self.args = args

    def calculate(self):
        """
        The returned object is of the form:
        {
            "heated_surface":   [m2]
            "heated_volume":    [m3]
        }

        Detailed Description:

        *********************************************************************
        Inputs:
        *********************************************************************
        g_surf                  [m2]        Building's ground surface
        nbr_floor               [unit]      Building's number of floors
        ratio_heated_surface    [ratio]     Ratio of heated surface divided by ground surface
                                            and divided by number of floors
        floor_height            [m]         Height of building's floors

        *********************************************************************
        Outputs:
        *********************************************************************
        heated_surface          [m2]        Building's heated surface
        heated_volume           [m3]        Building's heated volume

        """
        heated_surface = self.args["g_surf"] * self.args["nbr_floor"] * self.args["ratio_heated_surface"]

        h_vol = heated_surface * self.args["floor_height"]

        return {
            "heated_surface": heated_surface,
            "heated_volume": h_vol
        }

