from business.Base.Base import Base
from common.Guard import *
from math import sqrt, atan, pi
from common.IoC import *


class DetailedBuildingGeometry(Base):
    """
    Estimate wall length, wall surface, wall orientation, window ratio per wall and window surface per wall.
    """

    @staticmethod
    def help():
        return DetailedBuildingGeometry.__doc__ + "\r\n" + DetailedBuildingGeometry.calculate.__doc__

    @staticmethod
    def help_calculate():
        return DetailedBuildingGeometry.__init__.__doc__

    def __init__(self, args):
        """
        Arguments should be an object of the form:

        {"coord_x": [2571917.789, 2571917.9, 2571918.25, 2571919.25,
        2571919.25, 2571918.45, 2571917.45, 2571917.05,
        2571916.75, 2571916.05, 2571916.05, 2571917.05, 2571918.05,
        2571919.05, 2571917.789],
        "coord_y": [1105522.428, 1105522.428, 1105522.028, 1105521.168,
        1105521.118, 1105520.118,
        1105519.218, 1105519.218, 1105519.618, 1105519.918, 1105520.918,
        1105522.181, 1105522.981, 1105522.981,
        1105522.428],
        "nb_floor": 3}


        {
            "coord_x":    [dimensionless] (list)
                (must be a list and the same length as coord_y)
            "coord_y":    [dimensionless] (list)
                (must be a list and the same length as coord_x)
            "nb_floor":   [dimensionless]
                (must be positive integer)
        }

        IMPORTANT : Coordinates must be classed clock-wise.
        """
        # Add period and affectation for window rate and floor height variation

        super(DetailedBuildingGeometry, self).__init__(args)

        self.cache = Base.features.Require('Cache', HasMethods('get_file', 'get_object'))

        window_ratio_data = self.cache.get_object('WindowRatio.json')

        Guard.check_if_key_in_dict("coord_x", args)
        Guard.check_if_key_in_dict("coord_y", args)
        Guard.check_if_key_in_dict("nb_floor", args)

        Guard.check_is_higher(args["nb_floor"], lower_limit=0, strict=True)

        Guard.check_for_same_list_lengths(coord_x=args["coord_x"], coord_y=args["coord_y"])

        Guard.check_for_minimum_number_of_value_in_a_list(args["coord_x"], min=3)
        Guard.check_for_minimum_number_of_value_in_a_list(args["coord_y"], min=3)

        args["window_ratio_data"] = window_ratio_data

        self.args = args

    def calculate(self):
        """
        Estimate orientations and surfaces for building walls and windows.
        The returned object is of the form:
        {
            "wall_len":             [m]
            "th_br_int_fl"          [m]
            "wall_surf":            [m2]
            "orientation":          [deg.]
            "window":               [%]
            "win_surf":             [m2]
            "win_length"            [m]
            "win_height"            [m]
        }

        Detailed Description:

        *********************************************************************
        Inputs:
        *********************************************************************
        coord_x     [dimensionless] (list)    X coordinate of points
        coord_y     [dimensionless] (list)    Y coordinate of points
        nb_floor    [dimensionless]           Building floor number


        *********************************************************************
        Outputs:
        *********************************************************************
        wall_len                [m]   (list)          wall length
        th_br_int_fl            [m]   (list)          intermediate floor thermal bridges length
        wall_surf               [m2]  (list)          wall surface
        orientation             [deg.](list)          wall orientation
        window                  [%]   (list)          window ratio function of the wall orientation
        win_surf                [m2]  (list)          window surface function of the window ratio and the wall surface
        win_length              [m]   (list)          window length
        win_height              [m]   (list)          window height

        *********************************************************************
        Reference:
        *********************************************************************
        None

        """

        wall_len = []
        th_br_int_fl = []
        wall_surf = []
        orientation = []
        window = []
        win_surf = []
        win_length = []
        win_height = []
        orient = 0

        coord_x = self.args["coord_x"]
        coord_y = self.args["coord_y"]
        nb_floor = self.args["nb_floor"]

        height_build = nb_floor * 3

        # Length wall and intermediate thermal bridges calculation
        for x1, x2, y1, y2 in zip(coord_x[1:], coord_x[:-1], coord_y[1:], coord_y[:-1]):
            length_wall = sqrt((float(x1 - x2) ** 2 + (float(y1 - y2)) ** 2))
            len_int_floor_th_br = length_wall * (nb_floor - 1)
            wall_len.append(round(length_wall, 1))
            th_br_int_fl.append(round(len_int_floor_th_br, 1))

        # Wall surfaces calculation
        for val in wall_len:
            surface_wall = val * height_build
            wall_surf.append(round(surface_wall, 1))

        # Orientation calculation
        for x1, x2, y1, y2 in zip(coord_x[1:], coord_x[:-1], coord_y[1:], coord_y[:-1]):
            # testing points coordinates for calculation methodology
            if x1 == x2 and y1 < y2:
                orient = 90
            elif x1 == x2 and y1 > y2:
                orient = 270
            elif x1 > x2 and y1 == y2:
                orient = 0
            elif x1 < x2 and y1 == y2:
                orient = 180
            elif x1 > x2 and y1 < y2:
                orient = atan(abs(float(x1 - x2) / float(y1 - y2))) * 180 / pi
            elif x1 < x2 and y1 < y2:
                orient = atan(abs(float(x1 - x2) / float(y1 - y2))) * 180 / pi + 90
            elif x1 < x2 and y1 > y2:
                orient = atan(abs(float(x1 - x2) / float(y1 - y2))) * 180 / pi + 180
            elif x1 > x2 and y1 > y2:
                orient = atan(abs(float(x1 - x2) / float(y1 - y2))) * 180 / pi + 270
            orientation.append(round(orient, 1))

        window_ratio = 0
        # Window ratio affectation
        for i in orientation:
            for rng in self.args["window_ratio_data"]:
                if int(i) / 45 == self.args["window_ratio_data"][rng]["num"]:
                    window_ratio = self.args["window_ratio_data"][rng]["Win_Ratio"]
            window.append(window_ratio)

        # Window surface, length and height calculation
        for win, surf, w_len in zip(window, wall_surf, wall_len):
            surface_window = surf * win
            length_window = w_len * sqrt(win)
            height_window = height_build * sqrt(win)
            win_surf.append(round(surface_window, 1))
            win_length.append(round(length_window, 1))
            win_height.append(round(height_window, 1))

        return {
            "wall_len": wall_len,
            "ThBr_inter_floor": th_br_int_fl,
            "wall_surf": wall_surf,
            "orientation": orientation,
            "window": window,
            "win_surf": win_surf,
            "win_length": win_length,
            "win_height": win_height,
        }
