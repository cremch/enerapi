import unittest

from business.Base.Base import Base
from business.GeoBuilding.DetailedBuildingGeometry import DetailedBuildingGeometry
from common.DomainException import DomainException
from common.IoC import *
from data.InMemoryFileCache import InMemoryFileCache


class MockCache(InMemoryFileCache):
    def __init__(self):
        super(MockCache, self).__init__()
        self.ReadCount = 0

    # override
    def _load_file_from_disk(self, filename):
        WindowRatio = '{"Range0": {\
            "num": 0,\
            "Coord_Min": 0,\
            "Coord_Max": 44,\
            "Win_Ratio": 0.15\
        },\
        "Range1": {\
            "num": 1,\
            "Coord_Min": 45,\
            "Coord_Max": 89,\
            "Win_Ratio": 0.2\
        },\
        "Range2": {\
            "num": 2,\
            "Coord_Min": 90,\
            "Coord_Max": 134,\
            "Win_Ratio": 0.25\
        },\
        "Range3": {\
            "num": 3,\
            "Coord_Min": 135,\
            "Coord_Max": 179,\
            "Win_Ratio": 0.3\
        },\
        "Range3": {\
            "num": 3,\
            "Coord_Min": 180,\
            "Coord_Max": 224,\
            "Win_Ratio": 0.3\
        },\
        "Range5": {\
            "num": 5,\
            "Coord_Min": 225,\
            "Coord_Max": 269,\
            "Win_Ratio": 0.25\
        },\
        "Range6": {\
            "num": 6,\
            "Coord_Min": 270,\
            "Coord_Max": 314,\
            "Win_Ratio": 0.2\
        },\
        "Range7": {\
            "num": 7,\
            "Coord_Min": 315,\
            "Coord_Max": 359,\
            "Win_Ratio": 0.15\
        }}'

        mock_data = {}

        if filename == 'WindowRatio.json':
            mock_data = WindowRatio
        return mock_data


class TestGeoBuilding(unittest.TestCase):

    @staticmethod
    def setUpClass():
        Base.features = FeatureBroker()
        Base.features.Provide("Cache", MockCache)

    def test_if_all_arguments_are_here_no_exception_should_be_raised(self):
        coord_x = [2571917.789, 2571915.7833, 2571913.6373]
        coord_y = [1105522.4281, 1105520.7928, 1105523.4426]
        args = {"coord_x": coord_x, "coord_y": coord_y, "nb_floor": 2}
        DetailedBuildingGeometry(args)

    def test_if_any_required_argument_is_missing_an_exception_should_be_raised(self):
        coord_y = [1105522.4281, 1105520.7928, 1105523.4426]
        args = {"coord_y": coord_y, "nb_floor": 2}
        with self.assertRaises(DomainException):
            DetailedBuildingGeometry(args)

        coord_x = [2571917.789, 2571915.7833, 2571913.6373]
        args = {"coord_x": coord_x, "nb_floor": 2}
        with self.assertRaises(DomainException):
            DetailedBuildingGeometry(args)

        coord_x = [2571917.789, 2571915.7833, 2571913.6373]
        coord_y = [1105522.4281, 1105520.7928, 1105523.4426]
        args = {"coord_x": coord_x, "coord_y": coord_y}
        with self.assertRaises(DomainException):
            DetailedBuildingGeometry(args)

    def test_if_an_argument_has_an_incorrect_value_an_exception_should_be_raised(self):
        coord_x = [2571917.789, 2571915.7833, 2571913.6373]
        coord_y = [1105522.4281, 1105520.7928, 1105523.4426]
        args = {"coord_x": coord_x, "coord_y": coord_y, "nb_floor": 0}
        with self.assertRaises(DomainException):
            DetailedBuildingGeometry(args)

        coord_x = [2571917.789, 2571915.7833]
        coord_y = [1105522.4281, 1105520.7928, 1105523.4426]
        args = {"coord_x": coord_x, "coord_y": coord_y, "nb_floor": 2}
        with self.assertRaises(DomainException):
            DetailedBuildingGeometry(args)

        coord_x = [2571917.789]
        coord_y = [1105522.4281]
        args = {"coord_x": coord_x, "coord_y": coord_y, "nb_floor": 2}
        with self.assertRaises(DomainException):
            DetailedBuildingGeometry(args)

    def test_the_estimation_should_be_correct_case1(self):
        coord_x1 = [2571917.789, 2571917.9, 2571918.25, 2571919.25, 2571919.25, 2571918.45, 2571917.45, 2571917.05, 2571916.75, 2571916.05, 2571916.05, 2571917.05, 2571918.05, 2571919.05, 2571917.789]
        coord_y1 = [1105522.428, 1105522.428, 1105522.028, 1105521.168, 1105521.118, 1105520.118, 1105519.218, 1105519.218, 1105519.618, 1105519.918, 1105520.918, 1105522.181, 1105522.981, 1105522.981, 1105522.428]
        args1 = {"coord_x": coord_x1, "coord_y": coord_y1, "nb_floor": 3}

        expected1 = {
            "wall_len": [0.1, 0.5, 1.3, 0.1, 1.3, 1.3, 0.4, 0.5, 0.8, 1, 1.6, 1.3, 1, 1.4],
            "wall_surf": [0.9, 4.5, 11.7, 0.9, 11.7, 11.7, 3.6, 4.5, 7.2, 9, 14.4, 11.7, 9, 12.6],
            "orientation": [0, 41.2, 49.3, 90, 128.7, 138, 180, 216.9, 246.8, 270, 308.4, 321.3, 0, 156.3],
            "window": [0.15, 0.15, 0.2, 0.25, 0.25, 0.3, 0.3, 0.3, 0.25, 0.2, 0.2, 0.15, 0.15, 0.3],
            "win_surf": [0.1, 0.7, 2.3, 0.2, 2.9, 3.5, 1.1, 1.3, 1.8, 1.8, 2.9, 1.8, 1.3, 3.8],
            "win_length": [0, 0.2, 0.6, 0.1, 0.7, 0.7, 0.2, 0.3, 0.4, 0.4, 0.7, 0.5, 0.4, 0.8],
            "win_height": [3.5, 3.5, 4, 4.5, 4.5, 4.9, 4.9, 4.9, 4.5, 4, 4, 3.5, 3.5, 4.9],
            "ThBr_inter_floor": [0.2, 1.1,2.6, 0.1,2.6, 2.7, 0.8, 1, 1.5, 2, 3.2, 2.6, 2, 2.8]
        }

        actual1 = DetailedBuildingGeometry(args1).calculate()


        for idx, val in enumerate(expected1["wall_len"]):
            self.assertAlmostEqual(val, actual1["wall_len"][idx], places=2, msg=None, delta=None)
            self.assertAlmostEqual(expected1["ThBr_inter_floor"][idx], actual1["ThBr_inter_floor"][idx], places=2,
                                   msg=None, delta=None)
            self.assertAlmostEqual(expected1["wall_surf"][idx], actual1["wall_surf"][idx], places=2, msg=None, delta=None)
            self.assertAlmostEqual(expected1["orientation"][idx], actual1["orientation"][idx], places=2, msg=None,
                                   delta=None)
            self.assertAlmostEqual(expected1["window"][idx], actual1["window"][idx], places=2, msg=None, delta=None)
            self.assertAlmostEqual(expected1["win_surf"][idx], actual1["win_surf"][idx], places=2, msg=None, delta=None)
            self.assertAlmostEqual(expected1["win_length"][idx], actual1["win_length"][idx], places=2, msg=None,
                                   delta=None)
            self.assertAlmostEqual(expected1["win_height"][idx], actual1["win_height"][idx], places=2, msg=None,
                                   delta=None)

        pass
