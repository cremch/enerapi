import unittest

from business.Base.Base import Base
from business.GeoBuilding.EquivalentSouthArea import EquivalentSouthArea
from common.DomainException import DomainException
from common.IoC import *
from data.InMemoryFileCache import InMemoryFileCache


class MockCache(InMemoryFileCache):
    def __init__(self):
        super(MockCache, self).__init__()
        self.ReadCount = 0

    # override
    def _load_file_from_disk(self, filename):
        coef_orient = '{"Born1": {\
            "Level": 0,\
            "OrientMin": 0,\
            "OrientMax": 89,\
            "a": 0.004111111,\
            "b": 0.2\
        },\
        "Born2": {\
            "Level": 1,\
            "OrientMin": 90,\
            "OrientMax": 179,\
            "a": 0.005888889,\
            "b": 0.04\
        },\
        "Born3": {\
            "Level": 2,\
            "OrientMin": 180,\
            "OrientMax": 269,\
            "a": -0.005888889,\
            "b": 2.16\
        },\
        "Born4": {\
            "Level": 3,\
            "OrientMin": 270,\
            "OrientMax": 359,\
            "a": -0.004111111,\
            "b": 1.68\
        }}'

        win_type_rate = '{"wood": {\
            "DV": 0.517\
        }}'

        mock_data = {}

        if filename == 'WindowTypeIncidentRate.json':
            mock_data = win_type_rate
        if filename == 'SSE_Coef_Orient.json':
            mock_data = coef_orient
        return mock_data


class TestEquivalentSouthArea(unittest.TestCase):
    @staticmethod
    def setUpClass():
        Base.features = FeatureBroker()
        Base.features.Provide("Cache", MockCache)

    def test_if_all_arguments_are_here_no_exception_should_be_raised(self):
        win_surf = 23.290773387804258
        win_type = "wood"
        glass_type = "DV"
        orientation = 129.19126680283466
        fe1 = 0.8
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe1": fe1, "fe2": fe2}
        EquivalentSouthArea(args)

    def test_if_any_required_argument_is_missing_an_exception_should_be_raised(self):
        win_type = "wood"
        glass_type = "DV"
        orientation = 129.19126680283466
        fe1 = 0.8
        fe2 = 1
        args = {"win_type": win_type, "glass_type": glass_type, "orientation": orientation, "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        win_surf = 23.290773387804258
        glass_type = "DV"
        orientation = 129.19126680283466
        fe1 = 0.8
        fe2 = 1
        args = {"win_surf": win_surf, "glass_type": glass_type, "orientation": orientation, "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        win_surf = 23.290773387804258
        win_type = "wood"
        orientation = 129.19126680283466
        fe1 = 0.8
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "orientation": orientation, "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        win_surf = 23.290773387804258
        win_type = "wood"
        glass_type = "DV"
        fe1 = 0.8
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        win_surf = 23.290773387804258
        win_type = "wood"
        glass_type = "DV"
        orientation = 129.19126680283466
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        win_surf = 23.290773387804258
        win_type = "wood"
        glass_type = "DV"
        orientation = 129.19126680283466
        fe1 = 0.8
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe1": fe1}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

    def test_if_an_argument_has_an_incorrect_value_an_exception_should_be_raised(self):
        # window surface >=0
        win_surf = -23.290773387804258
        win_type = "wood"
        glass_type = "DV"
        orientation = 129.19126680283466
        fe1 = 0.8
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        # 0 < orientation < 360
        win_surf = 23.290773387804258
        win_type = "wood"
        glass_type = "DV"
        orientation = 450.19126680283466
        fe1 = 0.8
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        # 0 < Fe1 < 1
        win_surf = 23.290773387804258
        win_type = "wood"
        glass_type = "DV"
        orientation = 129.19126680283466
        fe1 = -0.8
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        # 0 < Fe2 < 1
        win_surf = 23.290773387804258
        win_type = "wood"
        glass_type = "DV"
        orientation = 129.19126680283466
        fe1 = 0.8
        fe2 = 1.5
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        # win_type value
        win_surf = 23.290773387804258
        win_type = "ood"
        glass_type = "DV"
        orientation = 129.19126680283466
        fe1 = 0.8
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

        # glass_type value
        win_surf = 23.290773387804258
        win_type = "wood"
        glass_type = "double"
        orientation = 129.19126680283466
        fe1 = 0.8
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe1": fe1, "fe2": fe2}
        with self.assertRaises(DomainException):
            EquivalentSouthArea(args)

    def test_the_estimation_should_be_correct_case1(self):
        win_surf = 23.290773387804258
        win_type = "wood"
        glass_type = "DV"
        orientation = 129.19126680283466
        fe1 = 0.8
        fe2 = 1
        args = {"win_surf": win_surf, "win_type": win_type, "glass_type": glass_type, "orientation": orientation,
                "fe1": fe1, "fe2": fe2}

        expected = {
            "esa": 7.7
        }

        actual = EquivalentSouthArea(args).calculate()

        self.assertAlmostEqual(expected["esa"], actual["esa"], places=2, msg=None, delta=None)

        pass
