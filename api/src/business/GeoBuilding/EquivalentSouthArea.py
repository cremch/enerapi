from business.Base.Base import Base
from common.Guard import *
from common.IoC import *


class EquivalentSouthArea(Base):
    """
    Estimate equivalent south area of a building from window surfaces, orientation, environmental parameters and
    window typologies.
    """

    _default_parameter_value = {}

    FRAME_TYPES = ["wood", "PVC", "metal", "rupt_metal"]
    GLASS_TYPES = ["SV", "DV", "DV_VIR", "TV", "TV_VIR"]

    @staticmethod
    def help():
        return EquivalentSouthArea.__doc__ + "\r\n" + EquivalentSouthArea.calculate.__doc__

    @staticmethod
    def help_calculate():
        return EquivalentSouthArea.__init__.__doc__.format(
            ", ".join(EquivalentSouthArea.FRAME_TYPES),
            ", ".join(EquivalentSouthArea.GLASS_TYPES)
        )

    def __init__(self, args):
        """
        Arguments should be an object of the form:

        {{"win_surf": 20, "win_type": "wood", "glass_type": "SV",
        "orientation": 90, "fe1": 0.9, "fe2": 0.8}}

        {{
            "win_surf":             [m2]
                (must be a positive integer)
            "win_type":             [dimensionless]
                (must be one of {0})
            "glass_type":           [dimensionless]
                (must be one of {1})
            "orientation":          [deg.]
                (must be between [0;359])
            "fe1":                  [dimensionless]
                (must be a close shade between 0 and 1)
            "fe2":                  [dimensionless]
                (must be a distant shade between 0 and 1)
        }}
        """

        super(EquivalentSouthArea, self).__init__(args)

        self.cache = Base.features.Require('Cache', HasMethods('get_file', 'get_object'))

        win_type_rate = self.cache.get_object('WindowTypeIncidentRate.json')
        coef_orient = self.cache.get_object('SSE_Coef_Orient.json')

        frame_list = win_type_rate.keys()
        glass_list = win_type_rate[frame_list[0]].keys()

        Guard.check_if_key_in_dict("win_surf", args)
        Guard.check_if_key_in_dict("win_type", args)
        Guard.check_if_key_in_dict("glass_type", args)
        Guard.check_if_key_in_dict("orientation", args)
        Guard.check_if_key_in_dict("fe1", args)
        Guard.check_if_key_in_dict("fe2", args)

        Guard.check_is_higher(args["win_surf"], lower_limit=0, strict=True)

        Guard.check_if_value_in_list(args["win_type"], values=frame_list)
        Guard.check_if_value_in_list(args["glass_type"], values=glass_list)

        Guard.check_value_in_between(args["orientation"], min=0, max=359)
        Guard.check_value_in_between(args["fe1"], min=0, max=1)
        Guard.check_value_in_between(args["fe2"], min=0, max=1)

        args["win_type_rate"] = win_type_rate
        args["coef_orient"] = coef_orient

        self.args = args

    def calculate(self):
        """

        Estimate equivalent south area for a building wall.
        The returned object is of the form:
        {
            "esa":             [m2]
        }

        Detailed Description:

        *********************************************************************
        Inputs:
        *********************************************************************
        win_surf             [m2]                window area
        win_type             [dimensionless]     window frame typology (wood, PVC, metal, metal_rupt)
        glass_type           [dimensionless]     window glass typology (SV, DV, DV_VIR, TV, TV_VIR)
        orientation          [deg.]              window orientation
        fe1                  [dimensionless]     close shade coefficient value
        fe2                  [dimensionless]     distant shade coefficient value


        *********************************************************************
        Outputs:
        *********************************************************************
        esa                  [m2]                 EquivalentSouthArea of a window


        *********************************************************************
        Reference:
        *********************************************************************
        "3CL-DPE Method v1.3" - part 4.2 : Method from official real estate energy label used in France. It allows to
        simplify thermal building models for solar gains in static thermal building calculation. Useful for building
        estimation at district or city scale approach.
        http://www.rt-batiment.fr/fileadmin/documents/RT_existant/DPE/DPE_outils/Annexe_methode_de_calcul_3CL-DPE_V1.3.pdf
        """

        win_surf = self.args["win_surf"]
        win_type = self.args["win_type"]
        glass_type = self.args["glass_type"]
        orientation = self.args["orientation"]
        fe1 = self.args["fe1"]
        fe2 = self.args["fe2"]

        def find_orientation(value):
            for born in self.args["coef_orient"]:
                if int(value) / 90 == self.args["coef_orient"][born]["Level"]:
                    return value * self.args["coef_orient"][born]["a"] + self.args["coef_orient"][born]["b"]

        cli = find_orientation(orientation)

        # Fei value estimation
        fei = fe1 * fe2

        # FTs value estimation
        fts = self.args["win_type_rate"][win_type][glass_type]

        # esa estimation
        esa = win_surf * cli * fei * fts

        return {
            "esa": round(esa, 1)
        }
