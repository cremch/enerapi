import unittest

from business.GeoBuilding.BuildingGeometry import BuildingGeometry
from common.DomainException import DomainException

__author__ = 'ppuerto'


class TestBaseGeoBuild(unittest.TestCase):
    def test_if_all_arguments_are_here_no_exception_should_be_raised(self):
        args = {"g_surf": 50, "nbr_floor": 2}
        BuildingGeometry(args)

    def test_if_any_required_argument_is_missing_an_exception_should_be_raised(self):
        args = {"g_surf": 50}
        with self.assertRaises(DomainException):
            BuildingGeometry(args)

        args = {"nbr_floor": 2}
        with self.assertRaises(DomainException):
            BuildingGeometry(args)

    def test_if_an_argument_has_an_incorrect_value_an_exception_should_be_raised(self):
        args = {"g_surf": -50, "nbr_floor": 2}
        with self.assertRaises(DomainException):
            BuildingGeometry(args)

        args = {"g_surf": 50, "nbr_floor": -2}
        with self.assertRaises(DomainException):
            BuildingGeometry(args)

        args = {"g_surf": 50, "nbr_floor": 2, "ratio_heated_surface": 0}
        with self.assertRaises(DomainException):
            BuildingGeometry(args)

        args = {"g_surf": 50, "nbr_floor": 2, "floor_height": 1}
        with self.assertRaises(DomainException):
            BuildingGeometry(args)

    def test_the_estimation_should_be_correct_case1(self):
        args = {"g_surf": 50, "nbr_floor": 2}

        expected = {
            "heated_surface": 80,
            "heated_volume": 200
        }

        actual = BuildingGeometry(args).calculate()

        self.assertAlmostEqual(expected["heated_surface"], actual["heated_surface"], places=0, msg=None, delta=None)
        self.assertAlmostEqual(expected["heated_volume"], actual["heated_volume"], places=0, msg=None, delta=None)

    def test_the_estimation_should_be_correct_case2(self):
        args = {"g_surf": 50, "nbr_floor": 2, "ratio_heated_surface": 0.5}

        expected = {
            "heated_surface": 50,
            "heated_volume": 125
        }

        actual = BuildingGeometry(args).calculate()

        self.assertAlmostEqual(expected["heated_surface"], actual["heated_surface"], places=0, msg=None, delta=None)
        self.assertAlmostEqual(expected["heated_volume"], actual["heated_volume"], places=0, msg=None, delta=None)

    def test_the_estimation_should_be_correct_case3(self):
        args = {"g_surf": 50, "nbr_floor": 2, "floor_height": 2}

        expected = {
            "heated_surface": 80,
            "heated_volume": 160
        }

        actual = BuildingGeometry(args).calculate()

        self.assertAlmostEqual(expected["heated_surface"], actual["heated_surface"], places=0, msg=None, delta=None)
        self.assertAlmostEqual(expected["heated_volume"], actual["heated_volume"], places=0, msg=None, delta=None)
