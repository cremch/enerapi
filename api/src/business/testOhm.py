import unittest
from business.Ohm import Ohm


class TestLoiOhm(unittest.TestCase):
    def test_calc_with_no_arguments_should_throw_an_error(self):
        with self.assertRaises(Exception):
            Ohm.calc({})

    def test_calc_with_one_arguments_should_throw_an_error(self):
        with self.assertRaises(Exception):
            Ohm.calc({"u": 10})

    def test_calc_with_three_arguments_should_throw_an_error(self):
        with self.assertRaises(Exception):
            Ohm.calc({"u": 1, "r": 1, "i": 1})

    def test_calc_r_with_u_and_i_arguments_should_work(self):
        self.assertEqual({"u": 6, "i": 2, "r": 3}, Ohm.calc({"u": 6, "i": 2, "r": None}))

    def test_calc_r_with_u_and_r_arguments_should_work(self):
        self.assertEqual({"u": 6, "i": 2, "r": 3}, Ohm.calc({"u": 6, "i": None, "r": 3}))

    def test_calc_r_with_r_and_i_arguments_should_work(self):
        self.assertEqual({"u": 6, "i": 2, "r": 3}, Ohm.calc({"u": None, "i": 2, "r": 3}))


if __name__ == '__main__':
    unittest.main()
