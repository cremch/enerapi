from common.IoC import FeatureBroker
from business.Base.Base import Base
from data.InMemoryFileCache import InMemoryFileCache


# Tell the IoC container that whenever someone needs a cache system, he'll get the InMemoryFileCache
Base.features = FeatureBroker()
Base.features.Provide('Cache', InMemoryFileCache)
