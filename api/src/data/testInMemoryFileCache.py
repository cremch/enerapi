__author__ = 'restani'

import os
from data.InMemoryFileCache import InMemoryFileCache
import unittest
import json

class TestFileCache(unittest.TestCase):

    def setUp(self):
        os.environ["ENERAPI_DATA_DIR"] = os.path.join(os.path.dirname(os.path.realpath(__file__)), "testData")

    def testReadingASmallFileShouldNotCrash(self):
        f = open(os.path.join(os.environ["ENERAPI_DATA_DIR"], "small_text_file.txt"), "rt")
        expected = f.read()
        f.close()
        actual = InMemoryFileCache().get_file("small_text_file.txt")
        self.assertEquals(expected, actual)
        pass

    def testTryingToGetANotExistingFileShouldReturnAnError(self):
        with self.assertRaises(Exception):
            InMemoryFileCache().get_file("not_existing_file.something")
        pass

    def testGettingAFileTwiceShouldOnlyReadItOnce(self):
        cache = MockCache()
        self.assertEquals(cache.ReadCount, 0)
        cache.get_file("whatever")
        self.assertEquals(cache.ReadCount, 1)
        cache.get_file("whatever")
        self.assertEquals(cache.ReadCount, 1)
        pass

    def testGettingAFileTwiceShouldReturnTheSameContent(self):
        once = InMemoryFileCache().get_file("small_text_file.txt")
        twice = InMemoryFileCache().get_file("small_text_file.txt")
        self.assertEquals(once, twice)
        pass

    def testNotSettingDataDirShouldUseTheFilesLocation(self):
        del os.environ["ENERAPI_DATA_DIR"]
        self.assertEquals(InMemoryFileCache()._dataDir,
                          os.path.dirname(os.path.realpath(__file__)).replace(os.sep + 'src', ''))
        pass

    def testGettingACorrectObjectShouldWork(self):
        # This is the content of the small_file.json file
        expected = dict(key_number=1, key_text="ghi", key_boolean=True, key_array=[1, 2, 3], key_dict={
            "abc": 123
        })
        actual = InMemoryFileCache().get_object("small_file.json")
        self.assertDictEqual(expected, actual)
        pass

    def testGettingACorrectObjectTwiceShouldReturnTheSameObject(self):
        once = InMemoryFileCache().get_object("small_file.json")
        twice = InMemoryFileCache().get_object("small_file.json")
        self.assertDictEqual(once, twice)
        pass

    def testJSONParsingShouldWork(self):
        json_str = '{"2001-2005": {\
            "GBAUP": 8020,\
            "hS_ratio": 1.587,\
            "em_techno": "RBT",\
            "t_supply": 40,\
            "em_techno_coef": 1.3\
        }}'

        json_dict = {"2001-2005": {
            "GBAUP": 8020,
            "hS_ratio": 1.587,
            "em_techno": "RBT",
            "t_supply": 40,
            "em_techno_coef": 1.3
        }}

        self.assertDictEqual(json_dict, json.loads(json_str))

class MockCache(InMemoryFileCache):

    def __init__(self):
        super(MockCache, self).__init__()
        self.ReadCount = 0

    # override
    def _load_file_from_disk(self, filename):
        self.ReadCount += 1
        return "mock data"
