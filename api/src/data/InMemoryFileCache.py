import os
import json
__author__ = 'restani'


class InMemoryFileCache(object):
    """
    Simple file cache. Loads files only once.
    """

    def __init__(self):
        try:
            self._dataDir = os.environ["ENERAPI_DATA_DIR"]
        except KeyError:
            print "WARN: Environment variable 'ENERAPI_DATA_DIR' not set. Using default directory."
            self._dataDir = os.path.dirname(os.path.realpath(__file__).replace(os.sep + 'src', ''))
        # print "CACHE using Data Directory: {0}".format(self._dataDir)
        self._cache = {}

    def get_file(self, filename):
        try:
            return self._cache[filename]
        except KeyError:
            self._cache[filename] = self._load_file_from_disk(filename)
            return self._cache[filename]

    def get_object(self, filename):
        content = self.get_file(filename)
        # print "CONTENT: ", content
        return json.loads(content)

    def _load_file_from_disk(self, filename):
        f = open(os.path.join(self._dataDir, filename), "rt")
        content = f.read()
        f.close()
        return content
