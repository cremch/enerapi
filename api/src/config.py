from flask import Flask
from flask_restplus import Api
import logging
import sys


class Config:
    pass


Config.app = Flask(__name__)
Config.api = Api(Config.app, version='1.1', title="Welcome to CREM's EnerAPI",
                 description='Simple but not simplistic energetic calculations',
                 default='Methods', default_label='''click on 'Methods' to list all methods''',
                 contact='the CREM: contact Jakob Rager ', contact_email='jakob.rager@crem.ch',
                 license='GPL-3.0 License', license_url='http://www.gnu.org/licenses/gpl.txt'
                 )
# adding logger
Config.app.logger.addHandler(logging.StreamHandler(sys.stdout))
Config.app.logger.setLevel(logging.DEBUG)
