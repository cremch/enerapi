import platform
from subprocess import call
from os import sep

os_name = platform.uname()[0]
server_script = 'scripts' + sep + 'server'

if 'Linux' in os_name:
	call(['bash',  server_script])
	
elif 'Windows' in os_name:
	call(server_script + '.bat')
	
else:
	raise Exception("Could not indentify os, found {}".format(os_name))
	