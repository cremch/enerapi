IF NOT EXIST venv\NUL (
	scripts\up.bat
	)

IF EXIST coverage_html\NUL (
	rmdir /s /q coverage_html 
	)

cmd /k "call venv\Scripts\activate & call scripts\cover.bat"